# Kittell Projects - Unix - Ubuntu - Public Snippets

As the name suggests this is a small subgroup of scripts within a larger set of scripts and plans.

## Intention
The intent of this project is to provide scripts specifically for Ubuntu/Debian/Raspbian(RaspberryPi) for public use and collaboration.

If you choose to use or borrow scripts you found here I respectfully ask that you provide credit to my site in the comments of your code. If you choose you can provide it to end user view as well.

The scripts found in this project may be written completely, partially or not at all by me. I attempt to give credit where credit is due.

I welcome helpful collaboration, notice I don’t say criticism.

*   Collaboration: to work jointly with others or together especially in an intellectual endeavor
    *   **URL:** https://www.merriam-webster.com/dictionary/collaborate
    *   **Website Title:** Merriam-Webster
    *   **Article Title:** Collaborate
    *   **Date Accessed:** July 11, 2018
*   Criticism: the act of criticizing usually unfavorably
    *   **URL:** https://www.merriam-webster.com/dictionary/criticism
    *   **Website Title:** Merriam-Webster
    *   **Article Title:** Criticism
    *   **Date Accessed:** July 11, 2018