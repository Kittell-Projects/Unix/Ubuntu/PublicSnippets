#!/bin/bash

if `service ssh status | grep -q "not running"`
then
    echo "Attempting to start ssh" 
    sudo service ssh start
else
    echo "SSH service is currently running" 
fi

# To Do: Need to account for when/if the service fails to start